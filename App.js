import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {StyleSheet} from 'react-native';
import Component1 from './src/component1';
import Component2 from './src/component2';
import Component3 from './src/component3';
// import LoginScreen from './src/Pages/LoginScreen';
// import RegisterScreen from './src/Pages/RegisterScreen';
import Styling1 from './src/Styling1';
import Router from './src/Router';
import LoginScreen from './src/screens/auth/LoginScreen';
import RegisterScreen from './src/Pages/RegisterScreen/index';

class App extends Component {
  render() {
    return (
      <>
        {/* <Text>Styling React</Text> */}
        {/* <Styling1 /> */}
        {/* <Component1 /> */}
        {/* <Component3 /> */}
        {/* <LoginScreen /> */}
        <RegisterScreen />
        {/* <Router /> */}
      </>
    );
  }
}

export default App;

const style = StyleSheet.create({});
