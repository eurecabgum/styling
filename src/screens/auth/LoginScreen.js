import React, {Component} from 'react';
import {Text, View, Stylesheet} from 'react-native';
import InputInline from '../../components/Input/InputInline';
import ButtonBlock from '../../components/buttons/ButtonBlock';

class LoginScreen extends Component {
  render() {
    return (
      <View>
        <Text>Please login with a registered account</Text>
        <InputInline Placeholder="UserName / Email" />
        <InputInline Placeholder="Password" />
        <ButtonBlock
          text="Login"
          color="blue"
          borderRadius={10}
          colorText="darkblue"
        />
      </View>
    );
  }
}

export default LoginScreen;
