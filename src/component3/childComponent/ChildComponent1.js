import React from 'react';
import {View, Text} from 'react-native';
import Avatar from '../child/Avatar';
import LengthChild from '../child/LengthChild';

const ChildComponent1 = () => {
  return (
    <View
      style={{
        marginTop: 20,
        backgroundColor: '#EECC8D',
        borderColor: 'black',
        borderWidth: 3,
        padding: 20,
      }}>
      <Text style={{alignSelf: 'center', color: 'black', fontSize: 20}}>
        {' '}
        Child Component
      </Text>
      <View style={{flexDirection: 'row'}}>
        <Avatar />
        <LengthChild
          color={'#D75404'}
          width={'70%'}
          text={'Child'}
          paddingLeft="20"
        />
      </View>
    </View>
  );
};

export default ChildComponent1;
