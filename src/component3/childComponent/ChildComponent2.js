import React from 'react';
import {View, Text} from 'react-native';
import LengthChild from '../child/LengthChild';

const ChildComponent2 = () => {
  return (
    <View
      style={{
        marginTop: 20,
        backgroundColor: '#EECC8D',
        borderColor: 'black',
        borderWidth: 3,
        padding: 20,
      }}>
      <Text style={{alignSelf: 'center', color: 'black', fontSize: 20}}>
        {' '}
        Child Component
      </Text>
      <View style={{flexDirection: 'row'}}>
        <LengthChild width={70} color={'#07000E'} text={'Child'} />
        <LengthChild width={70} color={'#07000E'} text={'Child'} />
        <LengthChild width={70} color={'#07000E'} text={'Child'} />
      </View>
    </View>
  );
};

export default ChildComponent2;
