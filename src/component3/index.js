import React, {Component} from 'react';
import {Text, View} from 'react-native';
import LengthChild from './child/LengthChild';
import ChildComponent1 from './childComponent/ChildComponent1';
import ChildComponent2 from './childComponent/ChildComponent2';

class Component3 extends Component {
  render() {
    return (
      <View style={{backgroundColor: '#07000E', flex: 1, padding: 10}}>
        <Text style={{color: 'white', alignSelf: 'center'}}>
          Parent Component
        </Text>
        <ChildComponent1 />
        <ChildComponent2 />
        <LengthChild width={'70%'} color={'#EECC8D'} text={'Child'} />
        <LengthChild width={'70%'} color={'#D75404'} text={'Child'} />
      </View>
    );
  }
}

export default Component3;
