import React from 'react';
import {View, Text} from 'react-native';

const LengthChild = ({width, color, text}) => {
  return (
    <View
      style={{
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        width: width,
        backgroundColor: color,
        height: 40,
        borderColor: 'black',
        borderWidth: 2,
        marginHorizontal: 20,
      }}>
      <Text style={{fontWeight: 'bold', color: 'white'}}>{text}</Text>
    </View>
  );
};

export default LengthChild;
