import React, {Component} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import Cbutton from '../components/Cbutton';
import CinputTextRegis from '../components/CinputText';
import CinputTextAuth from '../components/CinputTextAuth';
import CtextLink from '../components/CtextLink';
import Gap from '../components/Gap';

class RegisterScreen extends Component {
  render() {
    return (
      <View style={Style.container}>
        <View style={{alignItems: 'center'}}>
          <Image
            style={{width: 100, height: 100}}
            source={{
              uri:
                'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUREA8QFRIVFRcWFRUYFxUWGBYVFRUWFxgVFhgYHSghGxslGxUVITEhJSkrLi4uFx81ODMsNyguLisBCgoKDg0OGhAQGy0mHyYrLS0tMC0tLS0tLS0tLS0tLS0tLS0tLS0tLSstLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAcAAEAAgIDAQAAAAAAAAAAAAAABgcEBQECAwj/xABGEAABAwIBBwcJBgMHBQAAAAABAAIDBBEFBgcSITFBURMiYXGBkaEyM0JSYnKxssEUIzRzktFDgpMkRFNUdKLCF2PS8PH/xAAbAQEAAgMBAQAAAAAAAAAAAAAAAwQBAgUGB//EADMRAQACAQMCBAMGBgMBAAAAAAABAgMEETEFIRITMkFRcYEiQmGhscEUNFJykdEGM/AV/9oADAMBAAIRAxEAPwC8UBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQLoCAgICAgICAgICAgICAgICAgICAgICAg85p2sGk97WtG0uIA7ysxEzwI1iOcDD4bg1Ie4bow5/iBbxU9dLlt7NJvEI1XZ34xqgpJHdL3NZ4N0vorFen2+9LWcsezR1edmsd5uKnYOpzz3kgeCmroMcczLXzZaqoziYi7+86PusjHxBUkaPFHsx5lmFJllXnbXT9jg34ALf+GxR92GPFLxOVFd/n6v8Aqv8A3WfIxf0wx4pcsyqrhsrqrtkcfiU/h8X9MHilkRZb4i3ZXS9ug74tWP4XF/Sz47NjT5zMQbtlif70bf8AjZRzosUs+ZZt6TO9OPO0sLulrnM8DpKK3T6+0tvNlv6DO1Su1TQzxHjZr294N/BQW0GSOJ3bRlhJsMyuop9UVXESfRcdB36X2Kr2wZK8w2i0S3YKibOUBAQEBAQEBAQEBAQcEoIpj+cGjpiW8oZpB6EVnWPAu8kd91Zx6TJfvttDSbxCvcZzp1ctxA1kDejnv/U4W7gr2PQ0j1d0c5JQ2uxCWY6U80kh4vcXd19it1pWvphHM78ulJSSSnRiikeeDGlx8AlrVrzJCR0Gb3EJf7toDjI5rfDWfBQW1eKvu38Fm+pc0VQfO1ULPda5/wAdFQT1CvtDPlS20GaCEecrJj7rWN+N1HPULe0NvKhnR5p6IbZKp387B8GKOddk9tmfKh7jNZh/Cf8AqH9lj+Nynlw6vzV0B2faB1SfuCn8blPLhiy5pKQ+TPVN6zGf+AW0a/J7xB5UNfU5nh/CrnD34wflcFJHUJ96seU09XmnrG+bkp5O1zT4i3ipa6/HPMS18uUer8ja+G5fRy2G9gEg/wBhKnrqcVuJazS0NHIwg2cCDwIse4qeJatlhWUNVT+YqZWD1b3b+l1x4KK+HHf1QzFphNsGztTNsKuBkg9aPmO69E3B8FUyaCs+iUkZfin2B5a0VVZsc4a8/wAN/Md2X1HsJVHJp8lOYSRaJSJQthAQEBAQEBAQEFWZ6sQlYYIWSvbG9ry9oNg4gtA0rbRr2bF0dBSs7zMIcsqwoaKSVwjhje952NaCT4bB0ro2tWsbzKLlOsEzVVMlnVMjYG+qOe/wOiO8qlk19Y9MbpIxT7pzhGbiggsXRGZw9KU6Q/QLN8FTvq8tvfZJGOISqCnYwaMbGtbwaAB3BV5mZ5bvVYBAQEBAQEBAQEGHiGFQTi08EUg9poPcTsW9b2r6Z2YmIlD8WzWUclzCZYHeydNv6Xa+4hWaa7JXnu0nHCDY1mzrYbmMNnYN7DZ36HfQlXMetx257I5xzCHTwOY4tkY5rhta4FpHWCrcTEx2aLczMV8skdQySV72sMegHEu0bh1wL7tQ1Ll66lYmNoTYpWUqCUQEBAQEBAQEFR58PO03uSfM1dPp/FkOX2eOZH8TP+SPnWeoemvzYxcriXLTiAgICAgICAgICAgICAgIKSzzfj2/6dnzyLr6D/r+qDLy3eY/yarri+D1D1DmrbEtJc5KICAgICAgICCo8+Hnab3JPmaun0/iyHL7PHMj+Jn/ACR86z1D01+Zi5XEuWmEBAQEBAQEBAQEBAQEGDjGLQ00TpqiQMY3edpPBo3noC2pS152qxM7MmlnD2NeL2c0OF9tnAEX71iY2nZlS+eb8ez/AE7PnkXW0H/X9UGTlvcx8Z0Kp1uaXxgHpDXEj/cO9QdQnvVtiWguelEBAQEBAQEBBUefDztN7knzNXT6fxZDl9njmR/Ez/kj51nqHpr8zFyuJctMICAgICAgICAgICDgoIXlZnFpqW8cRE849Fp5jT7b/oLnqVrDpL5O89oaTeIVDiOKVOI1DBM8ve94Yxg1NZpG1mN3bdu3iV1a46YaT4UMzMy+j4Iw1rWjY0ADsFlwJnfusoJlfkLJXVzZXSCOBsTWkjW8uDnkho2DURrPcVcwaqMWPbbeUdqbyl+BYTDSxCCnbZjduu5LjtLjvJ1Ktkva9vFZvFdobBaMiAgICAgICAgqPPh52m9yT5mrp9P4shy+zxzI/iZ/yR86z1D01+Zi5XEuWmEBAQEBAQEBAQdZJA0EuIAG0k2A6yU234ENygzlUdPdsbjUSD0Y/Jv0yHV3XVrHo8l+e0NJyRCsMpMvKyru0v5KI/w47i44Odtd8OhdLFpMeP8AGUU3mUWVlqs7NBkuXP8At8reY24hB9J2x0nUNYHSTwXO12faPLhJSvutx7wASSABrJOoDrXLhPETM7Qr3K3OG1t4qEhzthm9Ee56x6dnWpq4/i7ui6Pa2183aPg3WbVr/sTXyOc50kkjySbk3da5PYtcnqUuq+GNRNacREQlSjc4QEBAQEBAQEFR58PO03uSfM1dPp/FkOX2eOZH8TP+SPnWeoemvzMXK4ly0wgICAgIOCUGBXY3TQi81TAz3ntB7r3W9cd7cRLG8I1iWc+gjvoPkmcNzGG36nWHcp66LLbns1nJCIYtncndcU1PHEPWeTI7uFgPFWqdPrHqlpOSfZCcWx6pqT/aKiSQeqTZo6mDV4K5TDSnphpMzPLWqVgQ3SrITI99dJpPBbSsP3j9mlb0GdPE7lV1OojHG0ct6Vm0rTxbLSjo2CGG0jmANbHHbRaBqALtg8SuR4LW7y7Ol6Vmzd9to/FW2UOVVRVkiR+jHuibcN/m3u7VNWkQ9HpOn4dPHaN5+MtXQUbppGQxi73uDR27+obexZmdo3W8uWMVJvbiH0FhlG2GJkLfJY0NHYLXVWZ3l4LLknJebz7spYaCAgICAgICAgqPPh52m9yT5mrp9P4shy+zxzI/iZ/yR86z1D01+Zi5XEuWmEHF0EZx/LyipSWvl5SQfw4+cQeBOwdpU+LS5MneIazaIQXE870xuKaljYNxkJee5tgO8q7Tp9fvSjnJKN1mX+IybatzRwY1jPEC/irFdJij2azeZaSpxWeTztTO/wB6R5+JU0Y6RxEMbyw7LdgQEBB3ZETsBWlsla8ytYdFnzeissyjpQHB0jdID0LkX6CRrt1d4VbJqY22q7On/wCP3nvltt8m9rsfnlYItMMhaLNijHJxgcLDb2kqjt33l3tP0/Bg9Ne/xlqwsrogtbNxkxyDftdQLSObzAdXJsO0ngT4BQZLb9oeW6trvOt5WPiPzlIsCyiZVSTNhF2QlrdP1ydK5HRq271pauznajSXwUrN+ZbxaqogICAgICAgIKjz4edpvck+Zq6fT+LIcvs8cyP4mf8AJHzrPUPTX5mLlcS5aZh4tikVNE6aeQMY3aTvO4AbyeAW1KWvO1WJmIUpljnDnqyY4C6Gn2WBs944vcNg9keK6+DR1x97d5Q2vuhSuNBG0VmeIdgw7ge5azescylrps1uKT/h3FO71StJzUj3WK9M1VuKS7tpHdA7VpOpos06HqrcxEfV3FEd7go51ce0LlP+O3+/d6to27ySo51V/Zex9B09fVMy9WwtGxoUVstrcy6OLQafF6aQ7qNbiIjgQEHZjCSGtBJJsABck8AAjE2isbys7IjIPQLaisaC8a2RbQ3g5/F3RuUN8ntDzPUeq+PfHhnt7z/p5508fewto4yWhzNOQjaWkkBnVzTdMVfdv0XR1vvmt7TtDjM75NT1x/B6ZTr3NPr+yyVC8+ICAgICAgICCo8+Hnab3JPmaun0/iyHL7OMyFO7laiS3NDGMv7RcXW7h4hOoT2rBiWbjmMRUsLp53aLG97jua0byVz8dLXt4apZnZQmU+Pz4hLyknNjF+TjvzWD6u4ldanlYK7e61p+m6jU94jaPxaxlEN5J8FrbVT7Q7WH/j+Kv/Zbd6tp2j0QoZzXn3dLH0zS04pH1egaNwCjm0z7rdcOOvprH+HKwkEBAQEBAQEG4yfybnq3WhZzAedI7UxvbvPQFi1oqp6rXYtPH2p7/COVs5MZIQUY0mjTmtrlcNfU0eiFXteZeW1fUMuonae1fgkS0UFPZ1/xw/IZ88is4vS9Z0P+Xn+6f2bnM7sqeuP4PWmVT69zT6/sslQvPiAgICAgICAgrHOzg81TUUkcETnaQe3SsdFpLmm7juAFz2LoaLJWlbTZFkjeYTTBMLhw+lEYcGsjaXSSHVpOtznu/wDeAVPJknLbdJSn3a8qgyxx91dPpm4hZcQxncN73D1j4DUrVPsV2h6nQdIrTa+bvPw+DSrV2+0MuPCp3C7aaocOIjeR4BN4RTqcMc3j/MPQYFVf5Sp/pP8A2WPFDT+Mwf1x/l1kwapaLupagDjyb/2TeG0arBM7ReP8sIi2o7RtCymiYmN4cIyICAgIMrD8PlndoQRPkdwaNnSTsA60mduUWbNTDG952WLk3m2a20la4PP+E080e87a7qGrrUNsvwee1fWrW+zh7fj7rAp4GsaGMa1rQLBoFgB0AKGZcK1ptO9p3lrcdyjp6Vt5pQHbmDW89TfqdS2rSZWdNo8uonakfX2ZuGVgmhjmAIEjGvAO0BwvY96xMbTsgy45x3mk+yp86/45v5DPnkVjH6Xqeh/y8/3S3OZ3ZU9cfwetMqn17mn1/ZZKhefEBAQEBAQEBAQVZnRyi0n/AGKI8xtjKR6TtoZ1DUeu3BT467d3pOj6KNvPt9EIw2gknlbDE273mw4DiTwAUkzt3dvNmrhpN78QujJnJGCkaCGh81udI4Am/s+qOpV7XmXjtX1DLqLd52j4JEtFEQEGhymyXhq2EOY1stuZIBZwO6/EdBW9bzC7pNdl09omJ7fBR1TA5j3MeLOY4tcOlpsfgrL2lLxesWjiXkjd3hic/wAhrnH2QXfBGtr1r6piG+w3IutmtanLGn0pDoDuOvwWk3iFHN1TTY+bb/JMsGzYxts6qlMh9Rl2N7TtPgtJy/Bx9R1zJbtijb58pvRUUUDNCKNkbBuAAHWVFMzLj3yXy23tO8tPjGWlHT3Dpg949CPnntI1DtK2ikytYOm6jN3iu0fGUCxzONUS3bTgQs4+VIe3YOzvUsY4h3dP0XFj75J8U/ki1BSSVM7YwXOklcAXEknXtcSeAuexb77Q6WW9MGKbcRD6CpYAxjY2+SxoaOposPgqvu8Je02tNp91SZ2PxrfyGfNIrGLh6rof8vP9zcZndlT1x/B60yqfXuafX9lkqF58QEBAQEBAQEGJi1YIYZJjsjY53cL2WYjedkmHH5mStPjL55mlc9xe83c4lzjxJNyrb31KxWsVjiFsZr8A5KH7TI37yYc32Yt36tvcoMlt+zy3WNX5mTy68R+qcqJxhAQYlfiUMI0ppo4xxc4C/VxWYiZ4SY8V8k7UiZaN+XtADb7TfqZIR3hq28uy5HStXP3f0VJlHVMlqppYzdj5C5p1i41a7FWKxtD1mkx2x4K0tzENc1xBBBsRrBWU8xvGyT4Zl5VwgNBiePaYAe9tvFazSJc3N0nBknfvH1bP/qhU/wCXg73/ALrTyoVv/hYv6pYdTnGrXeSYWe6y573ErPl1TU6Lpo53n6o/iGNVE3nqiV44FxDf0jV4LeIiOF7HpcOL0ViGAFlYdmMJIDQSSbADWSTsACMWtFY3lb+b/JL7K3l5gPtDxa3+G0+j1nf3KDJfftDyXU+ofxFvBT0x+aZqJyVPZ2D/AG1v5DPmerGL0vWdD/l5+c/s3GZ3ZU9cfwcsZVPr3NPqslQPPiAgICAgICAg0eW7CaCpA28k7w1nwW9PVC50+YjU0mfipDDImPmjZK7Rjc9oeeDSRdWJns9nntauO1qc7PomNoAAAsALAdA2Ko8DMzM7y7IwIInl3lYKNgjisah4u0HYxuzTcPgFJSnidPp2gnU23t6YU7WVb5XmSV7nvO1zjc9nAdAVjh67Hipjr4aRtDxRuICAgICAgz8HwiapfycEZcd52Nb0udsCxMxHKDUanHgr4rytnJHIqKktI+0k/r21M6GD67VBa+7yuu6lfUT4Y7V+CWKNzHSWQNBc4gAAkngBtKMxEzO0KAyjxM1NTLPuc6zOhjdTfAX7VbiNo2e60eDycNaf+3WjmuwzkqPlHCzpnF/8g1N8BftUGWd5eZ6xn8zUeGOI7Jio3KEBAQEBAQEBB51EQe1zHC7XAtI4gixWYZraazEw+fcbwx1NPJA/aw2B9Zp1td2iytRO8bveaXPXPii8e60822UfLw8hI776IW17Xx7A7pI2Hs4qHJXad3mOraPycnjr6Z/VM1E5LhxQfP2UWImoqZZifKeQ3oY3U0DsHircRtGz3ekwxhw1pHw/NrVlZEBAQEBBn4Xg09QbQQvf7VrNHW46liZiOVfPqsWGN72T7Ac2QFn1kml/22XDeov2nssorZfg4Wp63M9sMbfjKf0VFHEwMijaxg2BosFFMzPLh5Mlsk+K87yyFhoIIHnRyg5OL7LG77yUc+3ox8P5tnVdS4q+7tdH0fmX823EcfNXWTmEuqqhkDb2Ju8+qweUfp1kKa07Ru9DrNRGnxTefp81+wRBjQxos1oAA4ACwCqPC2tNp3l6IwICAgICAgICAgg+c3J3lovtMTbyxA6QG10e09o296lx227Ox0jWeVfy7cT+qrcLxB8ErZonWe03HAjeD0EKeY37PT58Nc1JpbiV65OY3HVwtlj1HY9u9jt7Sqtq7S8RqtNfT5Jpb6M6uP3b7eo75SsRyix+uPnD5xZsHUrkvoLlYBAQbDDcFqJ/MU8jx6wFm/qOpYmYjlXzarDh9dtkqwzNlUPsZ5Y4hwHPd9APFaTlj2cvN1zFXtjiZ/JL8JyAo4bF0ZlcN8huP0jV4KOcky5Obquoy9t9o/BKI4w0WaAANgAAA7Ao3OmZmd5d0YEHGlrtfWhs1WUmOR0kJlkNzsY3e924D6lZrXxSs6XS31GTwV+v4KKxCtfNI6aU3e83P0A6BsAVuI2e2w4q4aRSvELczd5N/ZoeUkH38oBcPUb6LOveenqVfJbednlOqazz8vhr6Y/9ul6jcsQEBAQEBAQEBAQcEIKczg5LfZpOWib/AGeQ/wBN59H3Tu7uCs0vvD1nS9f51fLv6o/Npcmseko5RLHradT2bnt/cbis2ruu6zSU1NPDbn2lduEYrFVQiWF12uFiN7TbW1w3FV7RMS8ZmwXwX8F47woXEaUxSyREWLHub3EgeCtRO73OC/jx1tHvDGRKICDNw7FZ4DpQTSR8QDqPW06isTETyhzabFmj7dYlKaHOXVN1SxwydNiw+GrwWk4ocvJ0PDb0zMNnHnU9ai19En7tWvlfirT0Gfa/5OJM6h9Gi75P2anlEdB+N/ya+pznVR8iGBnXpO+oW0YoWKdDwx6rTLS1uWVdJqdVPaODLM8Wi/itopWFzH0zTU4rv80ozf1jaenqK6qkcQ5wY0uJc52gCbNvtJLrdi0vHinaHL6pinNmpgwwh+UuPSVkxlk1NGqNm5jf3O8qSK7Rs7Gj0dNNTwxz7ylObfJPlHNrJ2/dtN4mn03D0yPVG7iVHkvt2cvq3UPDHk457+/+lqAKB5pygICAgICAgICAgICDwrKVkrHRyNDmOFnA7wVmJ2bUvalotXmFKZYZLPo33F3QOPMfw9h/T8VYreLPY6DX11Ndp9Ucx+7Byex2Wkk5SE6j5bD5LxwPTwK2tWJhPq9Jj1NPDbn2lmZYVMU8oq4NQlA5Rh8pkrRYg9BFrHoKxWJiNkPT6ZMVJw39uJ+MI+tnQEBAQEBAQEBBlVle+RrGE2jjbosYNjeJ6XE6yURY8NaWm0czzKU5C5GmpInqARTg80bDKf8Aw6d6jvfbs5fUupxijy8c/a/Rb8bA0AAAACwA1ADgFXeVmZmd5dkYEBAQEBAQEBAQEBAQEGPW0bJWOjlYHMcLFp2FZidm1L2pbxVnaVP5YZFyUpMkQL6fjtdH0P6Pa71YreJes0HVK548N+1v1RNbusICAgICAgICDkBGJmIjeVhZGZAlxE9a2zNrYTtd0ycB7PfwUV8m3aHA6h1aI3x4frP+lnsYAAAAANQA3BQPOTMz3l2RgQEBAQEBAQEBAQEBAQEBB1e0EWIBB2goRO3Cvcq83YdeWis120xHU0+4fRPRs6lNXJ8Xd0XWJptTN3j4/wC1aVVM+N5jkY5j27WuFiFM9JjyVyVi1Z3h5I3EBAQEBBn4Pg81U/k4Iy47zsa3pc7csTMRyg1Gqx4K+K8rYyUyGhpbSSWln9YjmsPsD6nX1KC2SZeW1vVMmo+zXtX9UtUbliAgICAgICAgICAgICAgICAgICDV43gEFU3RniDrbHDU9vuuGtZi0wsafVZcE745VplBm7nhu+nPLR8NQkHZsd2dynrkieXotL1nHk+zl7T+SGSRlpLXNLXDaCCCOsHYpHYraLRvWd3VGwg96KjkmdoQxve7g0E9/AdJThHly0xRvedlgZO5tSbPrXWH+Ew/M/6DvUVsvwcHV9a+7gj6z+yxaGhjhYI4Y2sYNgaLD/6oZmZ5cDJktknxXneWQsNBAQEBAQEBAQEBAQEBAQEBAQEBAQEBBrMXwGnqRaeFrjudscOpw1raLTCfDqsuGd6W2QfF81+11LP1Mk+j2/UKSMvxdrB1z2y1+sPbBc2LBZ1XKXH1Gc1va7aeyyWy/BpqOuXntijb8ZTrDsOigboQxMY3g0W7TxUUzM8uLlzXyz4rzuy1hGICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIP/9k=',
            }}
          />
          <Gap height={20} />
          <Text style={{fontSize: 20, fontWeight: 'bold'}}>
            Salt Academy App
          </Text>
        </View>
        <Gap height={30} />
        <Text>Please Login with a registered Account</Text>
        <Gap height={20} />
        <CinputTextRegis
          icon={'user'}
          Placeholder="UserName / Email"
          borderWidth={2}
        />
        {/* <CinputTextRegis icon={"user"} Placeholder="UserName / Email" borderWidth={2}/> */}
        <Gap height={20} />
        <CinputTextAuth Placeholder="Password" />
        <Gap height={30} />
        <Cbutton
          text="Login"
          color="deeppink"
          borderRadius={10}
          colorText="darkblue"
        />
        <Gap height={30} />
        <CtextLink
          text1="Forgot Password ?"
          text2="Reset Password"
          colorText2="blue"
        />
        <CtextLink
          text1="Don't have an Account ?"
          text2="Sign Up"
          colorText2="red"
        />
      </View>
    );
  }
}

export default RegisterScreen;

const Style = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 10,
    flex: 1,
    paddingTop: 40,
    paddingHorizontal: 30,
  },
});
