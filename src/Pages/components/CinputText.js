import React from 'react'
import { StyleSheet, Text, TextInput, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

const CinputText = ({icon, Placeholder, borderWidth}) => {
    return (
        <View style={{flexDirection:"row", alignItems:"center", }}>
            <Icon name={icon} size={30} color="black" />
            <View style={{borderColor:"black", borderBottomWidth:2, marginLeft:5, width:"90%", borderWidth:borderWidth}}>
                <TextInput style={{color:"black", fontSize:16}} placeholder={Placeholder}/>
            </View>
        </View>
    )
}

export default CinputText;

const styles = StyleSheet.create({})
