import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'

const Cbutton = ({borderRadius, color, text, colorText}) => {
    return (
        <TouchableOpacity>
         <View style={{ backgroundColor:color, borderRadius:borderRadius, height:40, justifyContent:"center"  }} >
            <Text
                style={{ color:colorText, fontSize: 20, textAlign: "center", fontWeight: "bold" }}>{text}</Text>
         </View>
        </TouchableOpacity>
    )
}

export default Cbutton

const styles = StyleSheet.create({

})
