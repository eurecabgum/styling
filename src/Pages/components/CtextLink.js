import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import Gap from './Gap'

const CtextLink = ({text1,text2, colorText2}) => {
    return (
        <View style={{ alignItems: "center", flexDirection:"row"}}>
            <Text>{text1} </Text>
            <TouchableOpacity >
                <Text style={{color:colorText2,justifyContent:"center"}}>{text2}</Text>
            </TouchableOpacity>
        </View>
    )
}

export default CtextLink
