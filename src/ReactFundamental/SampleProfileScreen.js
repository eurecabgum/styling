import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';

class SampleProfileScreen extends Component {
  render() {
    return (
      <View>
        <Text>Sample Profile Screen</Text>
      </View>
    );
  }
}

export default SampleProfileScreen;
const style = StyleSheet.create({});
