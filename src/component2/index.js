import React, {Component} from 'react';
import {Text, View} from 'react-native';

class Component2 extends Component {
  render() {
    return (
      <View style={{backgroundColor: 'blue', flex: 1, padding: 10}}>
        <Text style={{color: 'white', alignSelf: 'center'}}>
          Parent Component
        </Text>
        <View style={{marginTop: 20}}>
          <View
            style={{
              width: 120,
              height: 70,
              backgroundColor: 'yellow',
              alignSelf: 'center',
              borderColor: 'black',
              borderWidth: 2,
            }}></View>
          <View
            style={{
              marginTop: 20,
              width: '100%',
              backgroundColor: 'green',
              height: 30,
              borderColor: 'black',
              borderWidth: 2,
            }}></View>
          <View
            style={{
              marginTop: 20,
              width: '100%',
              backgroundColor: 'green',
              height: 30,
              borderColor: 'black',
              borderWidth: 2,
            }}></View>
          <View
            style={{
              marginTop: 20,
              width: '30%',
              backgroundColor: 'red',
              height: 30,
              borderColor: 'black',
              borderWidth: 2,
              alignSelf: 'flex-end',
            }}></View>
        </View>
      </View>
    );
  }
}

export default Component2;
